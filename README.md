# README #

### What is this repository for? ###

* The program which takes two string arguments on the command line. The program is designed to check whether the second string is a subsequence of the first (do not use substr, substring or other standard functions, including regular expression libraries). One condition: every occurrence of * in the second string means zero or more characters of the first string. If the program were entering the strings abcd and a * c, then the second string counts as a substring of the first. Additional functionality is authorized to treat the star as a normal character if it is preceded by a \, and the \ character is treated normally except when preceded by an asterisk.
* 1.0

### How do I get set up? ###

* It's maven app so you can build it by executing command : *mvn clean package* (this command will also run tests)
* After build, in directory *substring/target/appassembler/bin* you will find two scripts to run the application (one for windows one for linux).
* You can run app by executing one of scripts (substring-app.bat or substring-app). Script accepts two parameters and prints the result of their work to the console.
* Example of using: **substring-app.bat abcd a*d** where **abcd** is first parameter and **a*d** is second parameter, this command should print something like this:

```
#!java

------------------------------------
SUCCESS
'a*d' is substring of 'abcd'
------------------------------------
```