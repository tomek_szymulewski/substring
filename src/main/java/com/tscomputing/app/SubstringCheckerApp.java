package com.tscomputing.app;

import com.tscomputing.substring.SubstringChecker;

/**
 * Main class for performing substring checking.
 *
 * @author TomaSz
 */
public class SubstringCheckerApp {

    public static void main(String[] args) {
        SubstringChecker checker = new SubstringChecker();
        String substring = args[1];
        String container = args[0];
        boolean isSubString = checker.check(container, substring);

        System.out.println("------------------------------------");
        System.out.println((isSubString ? "SUCCESS" : "FAIL"));
        System.out.println("'" + substring + "' is " + (isSubString ? "" : "not ") + "substring of '" + container + "'");
        System.out.println("------------------------------------");
    }
}
