package com.tscomputing.substring;

import java.util.LinkedList;
import java.util.List;

/**
 * Helper class for checking substrings. It encapsulates all methods that operates on char array.
 *
 * @author TomaSz
 */
public class CheckerCharSequence implements CharSequence {

    private static final CheckerCharSequence EMPTY_SEQUENCE = new CheckerCharSequence("");
    private char[] charArray;
    private int startIndex;
    private int endIndex;

    public CheckerCharSequence(String str) {
        if (str == null) {
            throw new IllegalArgumentException("str cannot have null value");
        }
        charArray = str.toCharArray();
        startIndex = 0;
        endIndex = charArray.length;
    }

    private CheckerCharSequence(char[] charArray, int startIndex, int endIndex) {
        this.charArray = charArray;
        this.startIndex = startIndex;
        this.endIndex = endIndex;
    }

    @Override
    public int length() {
        return endIndex - startIndex;
    }

    @Override
    public char charAt(int index) {
        return charArray[calculateRealIndex(index)];
    }

    private int calculateRealIndex(int index) {
        return startIndex + index;
    }

    @Override
    public CheckerCharSequence subSequence(int start, int end) {
        if (start < 0) {
            throw new ArrayIndexOutOfBoundsException(start);
        }
        if (end > length()) {
            throw new ArrayIndexOutOfBoundsException(end);
        }
        int subLen = end - start;
        if (subLen < 0) {
            throw new ArrayIndexOutOfBoundsException(subLen);
        }
        return ((start == 0) && (end == length())) ? this
                : new CheckerCharSequence(charArray, calculateRealIndex(start), calculateRealIndex(end));
    }

    public CheckerCharSequence subSequence(int start) {
        if (start < 0) {
            throw new ArrayIndexOutOfBoundsException(start);
        }
        int subLen = length() - start;
        if (subLen < 0) {
            throw new ArrayIndexOutOfBoundsException(subLen);
        }
        return (start == 0) ? this : new CheckerCharSequence(charArray, calculateRealIndex(start), endIndex);
    }

    public char beginningChar() {
        return charArray[startIndex];
    }

    public boolean isEmpty() {
        return length() == 0;
    }

    public boolean startWith(CheckerCharSequence sequence) {
        if (sequence.length() > length()) {
            return false;
        }
        for (int i = 0; i < sequence.length(); i++) {
            if (charAt(i) != sequence.charAt(i)) {
                return false;
            }
        }
        return true;
    }

    public List<CheckerCharSequence> split(char breakChar, char cancelSpecialChar) {
        CheckerCharSequence sequence = trim(breakChar, cancelSpecialChar);
        int lastBreakCharIndex = 0;
        List<CheckerCharSequence> sequences = new LinkedList<>();
        boolean removeSpecial = false;
        for (int i = 0; i < sequence.length(); i++) {
            if (sequence.charAt(i) == breakChar) {
                if (sequence.charAt(i - 1) == cancelSpecialChar) {
                    removeSpecial = true;
                    continue;
                }
                if (lastBreakCharIndex < i) {
                    if (removeSpecial) {
                        sequences.add(sequence.subSequence(lastBreakCharIndex, i).removeSpecial(breakChar, cancelSpecialChar));
                    } else {
                        sequences.add(sequence.subSequence(lastBreakCharIndex, i));
                    }
                    removeSpecial = false;
                }
                lastBreakCharIndex = i + 1;
            }
        }
        if (removeSpecial) {
            sequences.add(sequence.subSequence(lastBreakCharIndex, sequence.length()).removeSpecial(breakChar, cancelSpecialChar));
        } else {
            sequences.add(sequence.subSequence(lastBreakCharIndex, sequence.length()));
        }

        return sequences;
    }

    private CheckerCharSequence removeSpecial(char specialChar, char cancelSpecialChar) {
        for (int i = 1; i < length(); i++) {
            if (charAt(i) == specialChar && charAt(i - 1) == cancelSpecialChar) {
                removeCharAt(i - 1);
            }
        }
        return this;
    }

    private void removeCharAt(int index) {
        char[] newCharArray = new char[length() - 1];
        for (int oldIndex = 0, newIndex = 0; oldIndex < length(); oldIndex++, newIndex++) {
            if (oldIndex == index) {
                newIndex--;
                continue;
            }
            newCharArray[newIndex] = charAt(oldIndex);
        }
        charArray = newCharArray;
        startIndex = 0;
        endIndex = newCharArray.length;
    }

    private CheckerCharSequence trim(char trimmer, char specialChar) {
        CheckerCharSequence sequence = this;
        if (sequence.startsWith(trimmer)) {
            sequence = sequence.length() == 1 ? EMPTY_SEQUENCE : sequence.subSequence(1).trim(trimmer, specialChar);
        }
        if (sequence.endsWith(trimmer)) {
            if (sequence.length() == 1) {
                sequence = EMPTY_SEQUENCE;
            } else if (sequence.charAt(sequence.length() - 2) != specialChar) {
                sequence = sequence.subSequence(0, sequence.length() - 1).trim(trimmer, specialChar);
            }
        }
        return sequence;
    }

    private boolean startsWith(char c) {
        return length() > 0 && charAt(0) == c;
    }

    private boolean endsWith(char c) {
        return length() > 0 && charAt(length() - 1) == c;
    }
}
