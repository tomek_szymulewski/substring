package com.tscomputing.substring;

import java.util.List;

/**
 * Class that encloses all the logic for check whether the second string is substring of the first.
 *
 * @author TomaSz
 */
public class SubstringChecker {

    private static final int DOES_NOT_CONTAIN = -1;
    private static final char WILDCARD_CHAR = '*';
    private static final char CANCEL_SPECIAL_CHAR = '\\';

    private char wildcardChar;
    private char cancelSpecialChar;

    public SubstringChecker() {
        this(WILDCARD_CHAR, CANCEL_SPECIAL_CHAR);
    }

    public SubstringChecker(char wildcardChar, char cancelSpecialChar) {
        this.wildcardChar = wildcardChar;
        this.cancelSpecialChar = cancelSpecialChar;
    }

    /**
     * Check whether the second string is substring of the first.
     *
     * @param container string which will be searched for substring
     * @param substring substring which need to be found
     * @return <code>true</code> if the second string is substring of the first, <code>false</code> otherwise
     */
    public boolean check(String container, String substring) {
        CheckerCharSequence containerSequence = new CheckerCharSequence(container);
        CheckerCharSequence substringSequence = new CheckerCharSequence(substring);
        List<CheckerCharSequence> substringSequences = substringSequence.split(wildcardChar, cancelSpecialChar);

        int lastIndex = 0;
        for (CheckerCharSequence next : substringSequences) {
            lastIndex = check(containerSequence.subSequence(lastIndex), next);
            if (lastIndex == DOES_NOT_CONTAIN) {
                return false;
            }
            lastIndex += next.length();
        }

        return true;
    }

    private int check(CheckerCharSequence containerSequence, CheckerCharSequence substringSequence) {
        //check obvious results
        if (substringSequence.isEmpty()) {
            return 0;
        }
        if (containerSequence.isEmpty() || substringSequence.length() > containerSequence.length()) {
            return DOES_NOT_CONTAIN;
        }

        return preformCheck(containerSequence, substringSequence);
    }

    private int preformCheck(CheckerCharSequence containerSequence, CheckerCharSequence substringSequence) {
        for (int i = 0; i < containerSequence.length() - substringSequence.length() + 1; i++) {
            if (substringSequence.beginningChar() == containerSequence.charAt(i)) {
                if (containerSequence.subSequence(i + 1).startWith(substringSequence.subSequence(1))) {
                    return i;
                }
            }
        }

        return DOES_NOT_CONTAIN;
    }
}
