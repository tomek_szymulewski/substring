package com.tscomputing.substring;

import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
 * Test that check many different substring findings from external file.
 * @author TomaSz
 */
public class SubstringCheckerMassiveTest {

    private static final int CONTAINER_INDEX = 0;
    private static final int SUBSTRING_INDEX = 1;
    private static final int SHOULD_PASS_INDEX = 2;

    private List<TestEntry> testEntries;

    @Before
    public void setUp() throws Exception {
        loadTestEntries();
    }

    @Test
    public void massiveTestOfCheck() throws Exception {
        testEntries.forEach(SubstringCheckerMassiveTest.TestEntry::performTest);
    }

    private void loadTestEntries() throws IOException {
        BufferedReader br = getBufferedReader();
        testEntries = new LinkedList<>();
        String line;
        while ((line = br.readLine()) != null) {
            String[] testEntryCsv = line.split(";");
            testEntries.add(new TestEntry(
                    testEntryCsv[CONTAINER_INDEX],
                    testEntryCsv[SUBSTRING_INDEX],
                    Boolean.parseBoolean(testEntryCsv[SHOULD_PASS_INDEX]))
            );
        }
    }

    private BufferedReader getBufferedReader() {
        return new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream("/testEntries.csv")));
    }

    private class TestEntry {
        private SubstringChecker substringChecker;
        private String container;
        private String substring;
        private boolean shouldPass;

        public TestEntry(String container, String substring, boolean shouldPass) {
            this.container = container;
            this.substring = substring;
            this.shouldPass = shouldPass;
            substringChecker = new SubstringChecker();
        }

        public void performTest() {
            assertThat(buildReason(), substringChecker.check(container, substring), is(shouldPass));
        }

        private String buildReason() {
            return "'" + substring + "' is " + (shouldPass ? "" : "not ") + "substring of '" + container + "'";
        }
    }
}