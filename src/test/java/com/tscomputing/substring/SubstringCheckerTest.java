package com.tscomputing.substring;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test methods naming convention is
 * [unitOfWork_StateUnderTest_ExpectedBehavior]
 *
 * @author TomaSz
 */
public class SubstringCheckerTest {

    private SubstringChecker substringChecker;

    @Before
    public void setUp() throws Exception {
        substringChecker = new SubstringChecker();
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkSubstring_NullArgument_ThrowException() {
        assertTrue(substringChecker.check("abcd", null));
    }

    @Test
    public void checkSubstring_SubstringExists_ReturnTrue() {
        assertTrue(substringChecker.check("abcd", "bc"));
    }

    @Test
    public void checkSubstring_SubstringDoesNotExist_ReturnFalse() {
        assertFalse(substringChecker.check("abdc", "xy"));
    }

    @Test
    public void checkSubstringWithWildcards_SubstringExists_ReturnTrue() {
        assertTrue(substringChecker.check("abcd", "a*c"));
    }

    @Test
    public void checkSubstringWithWildcards_SubstringDoesNotExist_ReturnFalse() {
        assertFalse(substringChecker.check("abcd", "a*v"));
    }

    @Test
    public void checkSubstringWithMixedWildcards_SubstringExists_ReturnTrue() {
        assertTrue(substringChecker.check("abc*d", "a*\\*d"));
    }

    @Test
    public void checkSubstringWithMixedWildcards_SubstringDoesNotExist_ReturnFalse() {
        assertFalse(substringChecker.check("abc*def", "a*\\*d\\**"));
    }
}